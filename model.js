const mongoose = require('mongoose');
//Define our Mongoose Schema. Defined structure for all projects
var adminSchema = new mongoose.Schema({
    name: {
    	type: String, 
    	required:[true, 'Name should contain lowercase letters'],
    	unique: false,
    	lowercase:true},
    email: {
    	type: String,
    	required: [true, "Email field can't be empty"],
    	trim:true,
    	unique: false,
    	message:""},
    active: {type: Boolean, default:true}
});
module.exports = mongoose.model('Admin', adminSchema);
