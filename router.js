const express = require('express');
const mongoose =  require ('mongoose');
const controllerAdmin=require("./controller");
const app = express();
const middlewarevalidation = require('./middlewarevalidation');

app.route("/") 
    .get(controllerAdmin.getAdmin)
    .post(middlewarevalidation, controllerAdmin.postAdmin)

app.route("/:_id")
    .put(middlewarevalidation, controllerAdmin.updateAdmin)
    .delete(controllerAdmin.deleteAdmin)

module.exports = app;