const Admin = require ('./model')

exports.updateAdmin = function (req,res) {
    var adminId = req.params._id;
    var update = req.body;
    console.log(adminId);
    console.log(update);
    Admin.findOneAndUpdate(adminId,update,{new:true},function(err,admin){
        if(err){
            return res.send("Error: "+ err.message);
        }
        console.log(admin);
        res.send(admin + "\n Actualizacion realizada con exito");
    });
}

exports.getAdmin = function (req,res) {
     Admin.find({}, function(err,admin){
        if(err){
            return res.send("Error: "+ err.message);
        }
        console.log(admin);
        return res.send(admin);
    });
}

exports.postAdmin = function (req,res) {
    var newAdmin = new Admin({   name: req.body.name,
                                 email:req.body.email,
                                 active:req.body.active
                             });
    console.log(newAdmin);
            newAdmin.save(function(err,admin){
                if(err){
                    return res.send("Error: "+ err.message + ".....");
                }
                console.log(admin);
                res.send(newAdmin + "\n Insert realizado con exito");
            });
}

exports.deleteAdmin = function (res,req){
    Admin.findOneAndRemove({_id:req.params._id},function(err,admin){
        if(err){
            return res.send("Error: "+ err.message);
        }
        console.log(admin);
        res.send(admin + "\n Borrado realizado con exito");
    });
}