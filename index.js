const express = require('express');
const mongoose =  require ('mongoose');
const bodyparser =  require ('body-parser');
const router = require('./router');
var {ObjectID} = require ('mongodb');
const mongodbRoute = 'mongodb://test:test@ds135820.mlab.com:35820/bd_test';
var db=mongoose.connection;
const app = express();

const port = 3001;
var documento;

app.use(bodyparser.urlencoded({
    extended:true
}));

app.use(bodyparser.json());
app.use(router);




/*MONGODB*/
mongoose.Promise = global.Promise
mongoose.connect(mongodbRoute, (err, resp) => {
    if (err) {
        return console.log(`Error al conectar a la base de datos: ${err}`)
    }
    app.listen(port, () => {
        console.log(`Servidor up en ${port}`);
    });
    
    console.log(`Conexión con Mongo correcta.`);

});


